# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod

MAIN_FILE = "Aesthesia_Groundcover_Reworked-52384-1-1-1699610966"
PATCH_FILE = "Aesthesia_Groundcover_Reworked_-_OpenMW_patch-52384-1-2-1699611224"


class Package(NexusMod, MW):
    NAME = "Aesthesia Groundcover Reworked"
    DESC = "Grass for Morrowind and Tamriel Rebuilt. Made for every region"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/52384"
    KEYWORDS = "openmw"
    LICENSE = "attribution-derivation"
    NEXUS_URL = HOMEPAGE
    TEXTURE_SIZES = "512 2048"
    NEXUS_SRC_URI = f"""
        https://www.nexusmods.com/morrowind/mods/52384?tab=files&file_id=1000040922
        -> {MAIN_FILE}.7z
        https://www.nexusmods.com/morrowind/mods/52384?tab=files&file_id=1000040923
        -> {PATCH_FILE}.7z
    """
    RDEPEND = """
        tr? ( landmasses/tamriel-rebuilt[preview?] )
        !!land-flora/ozzy-grass
        !!land-flora/vurts-groundcover[-minimal]
        !!land-flora/aesthesia-groundcover
    """
    IUSE = "tr preview"
    INSTALL_DIRS = [
        InstallDir("00 Core", S=MAIN_FILE),
        InstallDir(
            "02 Plugins - Vanilla Regions",
            GROUNDCOVER=[
                # One of these is BM related
                File("AesAdGrass_AC.esp"),
                File("AesAdGrass_AI.esp"),
                File("AesAdGrass_AL.esp"),
                File("AesAdGrass_BC.esp"),
                File("AesAdGrass_WG.esp"),
                File("AesGrass_GL.esp"),
                File("AesGrass_SV.esp"),
            ],
            S=MAIN_FILE,
        ),
        InstallDir(
            "02 Plugins - TR",
            GROUNDCOVER=[
                File("AesAdGrass_TR_AC.esp"),
                File("AesAdGrass_TR_AI.esp"),
                File("AesAdGrass_TR_AL.esp"),
                File("AesAdGrass_TR_BC.esp"),
                File("AesAdGrass_TR_WG.esp"),
                File("AesGrass_TR_GL.esp"),
                File("AesGrass_TR_SV.esp"),
                File("Rem_TR_AT.esp"),
            ],
            S=MAIN_FILE,
            REQUIRED_USE="tr",
        ),
        InstallDir(
            "02 Plugins - TR Preview",
            GROUNDCOVER=[
                File("AesAdGrass_TRp_AC.esp"),
                File("AesAdGrass_TRp_AI.esp"),
                File("AesAdGrass_TRp_AL.esp"),
                File("AesAdGrass_TRp_BC.esp"),
                File("AesAdGrass_TRp_WG.esp"),
                File("AesGrass_TRp_AT.esp"),
                File("AesGrass_TRp_GL.esp"),
                File("AesGrass_TRp_SV.esp"),
                File("Rem_TRp_AT.esp"),
                File("Rem_TRp_GM.esp"),
                File("Rem_TRp_RR.esp"),
                File("Rem_TRp_RR.esp"),
            ],
            S=MAIN_FILE,
            REQUIRED_USE="tr preview",
        ),
        InstallDir(
            "01 Textures - SD",
            S=MAIN_FILE,
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            "01 Textures - HD",
            S=MAIN_FILE,
            REQUIRED_USE="texture_size_2048",
        ),
        InstallDir(
            ".",
            S=PATCH_FILE,
        ),
    ]
