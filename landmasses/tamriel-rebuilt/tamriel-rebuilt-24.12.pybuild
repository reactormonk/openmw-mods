# Copyright 2019-2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Tamriel Rebuilt"
    DESC = "A project that aims to fill in the mainland of Morrowind"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org/
        http://www.nexusmods.com/morrowind/mods/42145
    """
    LICENSE = "tamriel-rebuilt-24.12"
    RESTRICT = "mirror"
    # A list of incompatible mods can be found at
    #   https://www.tamriel-rebuilt.org/incompatible-mods
    # A list of landmass conflicts can be found in the README
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        >=landmasses/tamriel-data-11
        music? ( media-audio/tamriel-rebuilt-music )
        dynamic_music? ( media-audio/tamriel-rebuilt-original-soundtrack )
        !!land-rocks/on-the-rocks
        firemoth? ( quests-misc/siege-at-fort-firemoth )
        !firemoth? ( !!quests-misc/siege-at-fort-firemoth )
    """
    KEYWORDS = "openmw"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/42145?tab=files&file_id=1000048954 -> Tamriel_Rebuilt-42145-24-12-1734909103.7z
        firemoth? ( https://www.nexusmods.com/morrowind/mods/42145?tab=files&file_id=1000048956 -> TR_Firemoth_Remover-42145-1-0-1734909247-1.7z )
        l10n_ru? ( https://www.nexusmods.com/morrowind/mods/55464?tab=files&file_id=1000050409 -> trrus24-55464-2025-02-15-1739615975.7z )
    """
    IUSE = "+music +dynamic_music firemoth l10n_ru"
    TIER = 2

    INSTALL_DIRS = [
        InstallDir(
            "00 Core",
            S="Tamriel_Rebuilt-42145-24-12-1734909103",
            PLUGINS=[
                File("TR_Mainland.esm"),
            ],
        ),
        InstallDir(
            "01 Faction Integration",
            S="Tamriel_Rebuilt-42145-24-12-1734909103",
            PLUGINS=[File("TR_Factions.esp")],
        ),
        InstallDir(
            "03 OpenMW",
            S="Tamriel_Rebuilt-42145-24-12-1734909103",
            PLUGINS=[File("tamrielrebuilt.omwscripts")],
        ),
        InstallDir(
            ".",
            S="TR_Firemoth_Remover-42145-1-0-1734909247-1",
            PLUGINS=[File("TR_Firemoth_remover.esp")],
            REQUIRED_USE="firemoth",
        ),
        InstallDir(
            "trrus24_2025-02-15/data files",
            S="trrus24-55464-2025-02-15-1739615975",
            BLACKLIST="Tamriel_*",
            PLUGINS=[
                File("TR_Mainland.esm"),
                File("TR_Factions.esp"),
            ],
            REQUIRED_USE="l10n_ru",
        ),
    ]
