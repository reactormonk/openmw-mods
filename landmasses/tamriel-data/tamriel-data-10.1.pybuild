# Copyright 2019-2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod

VANILLA_FILE = "Tamriel_Data_(Vanilla)-44537-10-1-1703373118"
HD_FILE = "Tamriel_Data_(HD)-44537-10-1-1703372988"


class Package(NexusMod, MW):
    NAME = "Tamriel Data"
    DESC = "Adds game data files required by several landmass addition mods"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org
        https://www.nexusmods.com/morrowind/mods/44537
    """
    # Unchanged since 10.0
    LICENSE = "tamriel-data-10.0"
    # weapon-sheathing-tr is included
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        !!gameplay-weapons/weapon-sheathing-tr
    """
    KEYWORDS = "openmw"
    NEXUS_SRC_URI = f"""
        texture_size_512? (
            https://www.nexusmods.com/morrowind/mods/44537?tab=files&file_id=1000041632
            -> {VANILLA_FILE}.7z
        )
        texture_size_1024? (
            https://www.nexusmods.com/morrowind/mods/44537?tab=files&file_id=1000041631
            -> {HD_FILE}.7z
        )
        devtools? (
            https://www.nexusmods.com/morrowind/mods/44537?tab=files&file_id=1000011260
            -> Tamriel_Data_Legacy-44537-06-01.7z
        )
    """
    IUSE = "devtools"
    RESTRICT = "fetch"
    TIER = 2

    TEXTURE_SIZES = "512 1024"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            ARCHIVES=[File("TR_Data.bsa"), File("PC_Data.bsa"), File("Sky_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=VANILLA_FILE,
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            ".",
            ARCHIVES=[File("TR_Data.BSA"), File("PC_Data.BSA"), File("Sky_Data.BSA")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=HD_FILE,
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                # Note that Tamriel_Data_Legacy should not be enabled in game.
                # This option is included as a development tool.
                # File("Tamriel_Data_Legacy.esm")
            ],
            S="Tamriel_Data_Legacy-44537-06-01",
            REQUIRED_USE="devtools",
        ),
    ]
