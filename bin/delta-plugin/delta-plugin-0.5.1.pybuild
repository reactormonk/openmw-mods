# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from pybuild.info import P, PV

from common.exec import Exec


class Package(Exec):
    NAME = "DeltaPlugin"
    DESC = "A tool for handling and converting markup-based versions of esp files"
    HOMEPAGE = "https://gitlab.com/bmwinger/delta-plugin"
    DOWNLOAD_ROOT = (
        "https://gitlab.com/api/v4/projects/18372672/packages/generic/delta-plugin"
    )
    SRC_URI = f"""
        platform_linux? ( {DOWNLOAD_ROOT}/{PV}/{P}-linux-amd64.zip )
        platform_win32? ( {DOWNLOAD_ROOT}/{PV}/{P}-windows-amd64.zip )
        platform_darwin? ( {DOWNLOAD_ROOT}/{PV}/{P}-darwin-amd64.zip )
    """
    LICENSE = "GPL-3"
    KEYWORDS = "openmw"
    IUSE = "platform_linux platform_win32 platform_darwin"

    def src_prepare(self):
        super().src_prepare()
        os.makedirs("bin")
        exe_name = (
            "delta_plugin.exe" if "platform_win32" in self.USE else "delta_plugin"
        )
        path = os.path.join(self.WORKDIR, self.S, "bin", exe_name)
        os.rename(exe_name, path)
